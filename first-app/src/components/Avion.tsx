import { IonCard, IonCardContent, IonCardTitle } from "@ionic/react";
import { Link } from "react-router-dom";

interface ContainerProps {
  id: number;
  marque: string;
  immatriculation: string;
}

const Avion: React.FC<ContainerProps> = ({ id, marque, immatriculation }) => {
  return (
    <div key={id}>
      <IonCard>
        <IonCardTitle>{marque}</IonCardTitle>
        <IonCardContent>Immatriculation : {immatriculation}</IonCardContent>
        <IonCardContent>
          <Link to={"/avions/" + id}>Detail</Link>
        </IonCardContent>
      </IonCard>
    </div>
  );
};

export default Avion;
