import { useState } from "react";
import { Redirect } from "react-router-dom";
import Avion from "./Avion";
import axios from "axios";
import { useIonViewWillEnter } from "@ionic/react";

const Avions: React.FC = () => {
  const [data, setData] = useState<any[]>([]);
  const [redirect, setRedirect] = useState(false);
  const [afficher, setAfficher] = useState(false);

  useIonViewWillEnter(() => {
    let hash = sessionStorage.getItem("token");
    //let hash = "18d0acd306d39604167b862c0d9f9b697c880381";

    axios
      .get(
        //18d0acd306d39604167b862c0d9f9b697c880381
        "http://flotteback-production.up.railway.app/avions?hash=" + hash
      )
      // .get(
      //   //18d0acd306d39604167b862c0d9f9b697c880381
      //   "http://localhost:8080/avions?hash=" + hash
      // )
      .then((response) => {
        let x = response.data.data;
        setData(x);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          sessionStorage.removeItem("token");
          alert(error.response.data.message);
          setRedirect(true);
        }
      });
  });

  console.log(data);

  return redirect ? (
    <Redirect to="/login" />
  ) : (
    <div>
      {data.map((vecl, index) => {
        return (
          <Avion
            key={vecl.id}
            id={vecl.id as number}
            marque={vecl.marque}
            immatriculation={vecl.matricule}
          />
        );
      })}
    </div>
  );
};
export default Avions;
