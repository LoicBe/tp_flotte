import { IonCard, IonCardContent, IonCardTitle } from "@ionic/react";

interface ContainerProps {
  id: number;
  marque: string;
  immatriculation: string;
  debut: string;
  fin: string;
}
const Assurance: React.FC<ContainerProps> = ({
  id,
  marque,
  immatriculation,
  debut,
  fin,
}) => {
  return (
    <li key={id}>
      <IonCard>
        <IonCardTitle>
          {marque} immatriculé : {immatriculation}
        </IonCardTitle>
        <IonCardContent>
          <p>debut d'assurance : {debut}</p>
          <p>fin d'assurance : {fin}</p>
        </IonCardContent>
      </IonCard>
    </li>
  );
};
export default Assurance;
