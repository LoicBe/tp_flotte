const listAvions = [
  {
    id: 1,
    marque: "NISSAN",
    immatriculation: "12BA",
    kilometrage: 200,
  },
  {
    id: 2,
    marque: "KIA",
    immatriculation: "TAR2",
    kilometrage: 250,
  },
  {
    marque: "FERRARI",
    immatriculation: "56TA",
    kilometrage: 220,
  },
  {
    id: 4,
    marque: "MITSUBISHI",
    immatriculation: "ww15",
    kilometrage: 225,
  },
];

export default listAvions;
