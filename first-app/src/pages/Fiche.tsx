import { Link, Redirect } from "react-router-dom";
import {
  IonButton,
  IonCard,
  IonCardContent,
  IonCardTitle,
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import axios from "axios";
import { useEffect, useState } from "react";
import { RouteComponentProps } from "react-router";
import "../styles/Tab3.css";

interface FicheProps
  extends RouteComponentProps<{
    id: string;
  }> {}

const Fiche: React.FC<FicheProps> = ({ match }) => {
  let ref = parseInt(match.params.id);
  const [Avion, setAvion] = useState<any>({});
  const [redirect, setRedirect] = useState(false);

  useEffect(() => {
    let hash = sessionStorage.getItem("token");
    //let hash = "18d0acd306d39604167b862c0d9f9b697c880381";
    axios
      .get(
        "http://flotteback-production.up.railway.app/avions/" + ref + "?hash=" + hash
      )
      //.get("http://localhost:8080/avions/" + ref + "?hash=" + hash)
      .then((response) => {
        console.log(response.data.data);
        let data = response.data.data;
        setAvion(data);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          sessionStorage.removeItem("token");
          alert(error.response.data.message);
          setRedirect(true);
        }
      });
  }, [ref]);

  return redirect ? (
    <Redirect to="/login" />
  ) : (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Fiche</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonCard>
          <IonCardTitle>Marque : {Avion.marque}</IonCardTitle>
          <IonCardContent>
            <p>Immatriculation : {Avion.matricule}</p>
            <p>Kilometrage : {Avion.kilometre}</p>
          </IonCardContent>
          <IonCardTitle>Assurance</IonCardTitle>
          <IonCardContent>
            <p>debut : {Avion.debutAssurance}</p>
            <p>fin : {Avion.finAssurance}</p>
          </IonCardContent>
        </IonCard>
        <Link to="/avions">Retour</Link>
      </IonContent>
    </IonPage>
  );
};

export default Fiche;
