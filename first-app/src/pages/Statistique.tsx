import {
  IonContent,
  IonHeader,
  IonPage,
  IonTitle,
  IonToolbar,
  useIonViewWillEnter,
} from "@ionic/react";
import axios from "axios";
import { useState } from "react";
import Assurance from "../components/Assurance";
import { Redirect } from "react-router";

const Statistique: React.FC = () => {
  const [redirect, setRedirect] = useState(false);
  const [data1, setData1] = useState<any[]>([]);
  const [data2, setData2] = useState<any[]>([]);

  useIonViewWillEnter(() => {
    let hash = sessionStorage.getItem("token");
    //let hash = "18d0acd306d39604167b862c0d9f9b697c880381";
    axios
      .get(
        "http://flotteback-production.up.railway.app/statistiques?hash=" + hash
      )
      //.get("http://localhost:8080/statistiques?hash=" + hash)
      .then((response) => {
        let x = response.data.data;
        setData1(x[0]);
        setData2(x[1]);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          sessionStorage.removeItem("token");
          alert(error.response.data.message);
          setRedirect(true);
        }
      });
  });

  console.log(data1);
  console.log(data2);

  return redirect ? (
    <Redirect to="/login" />
  ) : (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Page de statistique</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        {data1.length !== 0 ? (
          data1.map((vehicule) => {
            return (
              <div key={vehicule.vehicule.id}>
                <h3>Liste vehicule en fin d'assurance dans deux mois</h3>
                <ul>
                  <Assurance
                    key={vehicule.idVehicule}
                    id={vehicule.idVehicule}
                    marque={vehicule.vehicule.marque}
                    immatriculation={vehicule.vehicule.matricule}
                    debut={vehicule.debut}
                    fin={vehicule.fin}
                  />
                </ul>
              </div>
            );
          })
        ) : (
          <p>aucun vehicule en fin d'assurance dans deux mois</p>
        )}
        {data2.length !== 0 ? (
          data2.map((vehicule, index) => {
            return (
              <div key={vehicule.vehicule.id}>
                <h3>Liste vehicule en fin d'assurance dans trois mois</h3>
                <ul>
                  <Assurance
                    id={vehicule.vehicule.id}
                    marque={vehicule.vehicule.marque}
                    immatriculation={vehicule.vehicule.matricule}
                    debut={vehicule.debut}
                    fin={vehicule.fin}
                  />
                </ul>
              </div>
            );
          })
        ) : (
          <p>aucun vehicule en fin d'assurance dans trois mois</p>
        )}
      </IonContent>
    </IonPage>
  );
};
export default Statistique;
