import {
  IonButton,
  IonCard,
  IonContent,
  IonHeader,
  IonInput,
  IonItem,
  IonLabel,
  IonPage,
  IonTitle,
  IonToolbar,
} from "@ionic/react";
import axios from "axios";
import "../styles/login.css";
import { Redirect } from "react-router-dom";
import { useState } from "react";
import { useIonViewWillEnter } from "@ionic/react";

const Login: React.FC = () => {
  const [redirect, setRedirect] = useState(false);
  const [logIn, setLogIn] = useState(false);

  useIonViewWillEnter(() => {
    const token = sessionStorage.getItem("token");
    if (token === null) {
      setLogIn(false);
    } else {
      setLogIn(true);
    }
  });

  const admin = {
    email: "admin@gmail.com",
    password: "admin",
  };

  const submitForm = () => {
    axios
      .post("http://flotteback-production.up.railway.app/login", admin)
      //.post("http://localhost:8080/login", admin)
      .then((response) => {
        const data = response.data.data[1];
        sessionStorage.setItem("token", data.value);
        setRedirect(true);
      })
      .catch((error) => {
        if (error.response.status === 401) {
          setRedirect(false);
          alert(error.response.data.message);
        }
      });
  };

  const logOut = () => {
    sessionStorage.removeItem("token");
    alert("deconnecté");
    window.location.reload();
  };

  return logIn ? (
    <a onClick={logOut}>Session active, cliquer ici pour se deconnecter?</a>
  ) : redirect ? (
    <Redirect to="/avions" />
  ) : (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Login</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent fullscreen>
        <IonCard>
          <div className="content-login">
            <IonItem>
              <IonLabel>Email</IonLabel>
              <IonInput
                placeholder="email valide = admin@gmail.com"
                type="email"
                name="email"
                value={admin.email}
                onIonChange={(ev) => (admin.email = ev.detail.value!)}
              ></IonInput>
            </IonItem>
            <IonItem>
              <IonLabel>Password</IonLabel>
              <IonInput
                placeholder="password valide = admin"
                type="password"
                name="password"
                value={admin.password}
                onIonChange={(e) => (admin.password = e.detail.value!)}
              ></IonInput>
            </IonItem>
            <center>
              <IonButton
                size="small"
                className="login-log"
                onClick={submitForm}
              >
                Log In
              </IonButton>
            </center>
          </div>
        </IonCard>
      </IonContent>
    </IonPage>
  );
};

export default Login;
